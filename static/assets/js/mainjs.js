$(window).load(function() {
  /*function to auto-generate a CRSF cookie */
  function getCookie(name) {
      var cookieValue = null;
      if (document.cookie && document.cookie != '') {
          var cookies;
          cookies = document.cookie.split(';');
          for (var i = 0; i < cookies.length; i++) {
              var cookie = jQuery.trim(cookies[i]);
              // Does this cookie string begin with the name we want?
              if (cookie.substring(0, name.length + 1) == (name + '=')) {
                  cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                  break;
              }
          }
      }
      return cookieValue;
  }
  var csrftoken = getCookie('csrftoken');


    // Animate loader off screen
  $(".sk-cube-grid").fadeOut(3000,function(){
    document.getElementById("wrapper").classList.remove("wrapper-hide");
  });

  var contactModal = $('#contactModal');
  $('.showmodal').click(function(){
    contactModal.modal('show');
    $(".sk-circle").fadeOut(3000,function(){
      $(".contact-modal-text").text("Thanks Gusta for Reaching out!")
      $(".contact-modal-title").text("Received")
    });
  });

  $(".contact-send").click(function(){
    var name = $("#name").val();
    var email = $("#email").val();
    var subject = $("#subject").val();
    var message = $("#message").val();
    if (name ==="" || email ==="" || subject==="" || message==""){
      contactModal.modal('show');
      /*$(".sk-circle").fadeOut(100,function(){*/
        $(".contact-modal-text").text("Please Fill all required field")
        $(".contact-modal-title").text("Error")
      /*});*/
    }else {
      $.post("/contact",{csrfmiddlewaretoken: csrftoken,"name":name,"email":email,"subject":subject,"message":message},
             function(data){
                  document.getElementById("contactusform").reset();
                  contactModal.modal('show');
                  $(".contact-modal-text").text(data)
                  $(".contact-modal-title").text("Received")


       });
    }
});

/** Form Js **/
$(document).ready(function () {

    var navListItems = $('div.setup-panel div a'),
            allWells = $('.setup-content'),
            allNextBtn = $('.nextBtn');

    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
                $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-success').addClass('btn-default');
            $item.addClass('btn-success');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });

    allNextBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='email'],input[type='number'],input[type='url']"),
            isValid = true;

        $(".form-group").removeClass("has-error");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }

        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });

    $('div.setup-panel div a.btn-success').trigger('click');
});
/** End of Form Js**/
});
