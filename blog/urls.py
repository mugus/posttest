from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import url
from django.urls import path
from . import views
from .views import (PostListView,
					PostDetailView,
					PostCreateView,
					PostUpdateView,
					PostDeleteView)

urlpatterns = [
		#url('^$', views.home, name='blog-home'),\
		#url(r'^post/<int:pk>/$', PostDetailView.as_view(), name='post-detail'),
		#url(r'post/(?P<pk>\d+)/', PostDetailView.as_view(), name='post-detail'),
		#url(r'^post/(?P<pk>\d+)/update$', PostUpdateView.as_view(), name='post-update'),
		url('^$', PostListView.as_view(), name='blog-home'),
		path(r'post/<int:pk>/', PostDetailView.as_view(), name='post-detail'),
		path(r'post/<int:pk>/update', PostUpdateView.as_view(), name='post-update'),
		path(r'post/<int:pk>/delete', PostDeleteView.as_view(), name='post-delete'),
		url('^post/new/$', PostCreateView.as_view(), name='post-create'),
		url('^about$', views.about, name='blog-about'),
		url('^profile$', views.profile, name='blog-profile'),
		url('^contactus$', views.contactus, name='blog-contactus'),
		url(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', views.activate, name='activate'),
]
if settings.DEBUG:
	urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)



