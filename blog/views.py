from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.utils import timezone
from .models import Post
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.contrib import messages
from .forms import UserCreationForm, UserUpdateForm, ProfileUpdateForm
from django.views.generic import (ListView, DetailView, CreateView, UpdateView, DeleteView)
from django.template.loader import render_to_string, get_template
from django.core.mail import BadHeaderError, send_mail
from django.http import HttpResponse, HttpResponseRedirect
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode 
from .tokens import account_activation_token
from django.contrib.auth import get_user_model
# from django.utilities import account_activation_token
# from blog.models import User


# def contactus(request):
#     subject = request.POST.get('subject', 'test')
#     message = request.POST.get('message', 'check')
#     from_email = request.POST.get('from_email', 'muhozagustave1213@gmail.com')
#     if subject and message and from_email:
#         try:
#             send_mail(subject, message, from_email, ['muhozagustave1213@gmail.com'])
#         except BadHeaderError:
#             return HttpResponse('Invalid header found.')
#         return HttpResponse('sent')
#     else:
#         # In reality we'd use a form class
#         # to get proper validation errors.
#         return HttpResponse('Make sure all fields are entered and valid.')

# Create your views here.
"""
posts = [
	{
		'author': 'Gustavo',
		'title': 'Blog Post 1',
		'content': 'First post content',
		'date_posted': 'August 27, 2019'
	},
	{
		'author': 'Mugus',
		'title': 'Blog Post 2',
		'content': 'Second post content',
		'date_posted': 'August 28, 2019'
	}
]
"""

def contactus(request):
	send_mail(
    'confirm your account',
    'jhasfajgfas asvfjasfgsah asfgasjf asjfgjas',
    'muhozagustave1213@gmail.com',
    ['muhozagustave1213@gmail.com'],
    fail_silently=False,
	)
	return HttpResponse('sent')


def home(request):
	context = {
	'posts': Post.objects.all()
	}
	return render(request, 'blog/home.html', context)

class PostListView(ListView):
	model = Post
	template_name = 'blog/home.html'
	context_object_name = 'posts'
	ordering  = ['-date_posted']

class PostDetailView(DetailView):
	model = Post
		

class PostCreateView(LoginRequiredMixin, CreateView):
	model = Post
	fields = ['title', 'content']

	def form_valid(self, form):
		form.instance.author = self.request.user
		return super().form_valid(form)

class PostUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
	model = Post
	fields = ['title', 'content']

	def form_valid(self, form):
		form.instance.author = self.request.user
		return super().form_valid(form)
	def test_func(self):
		post = self.get_object()
		if self.request.user == post.author:
			return True
		return False

class PostDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
	model = Post
	success_url = '/'

	def test_func(self):
		post = self.get_object()
		if self.request.user == post.author:
			return True
		return False
		

#def profile(request):
#	return render(request, 'blog/profile.html', {'title': 'profile'})


def about(request):
	return render(request, 'blog/about.html', {'title': 'About'})

def register(request):
	if request.method == 'POST':
		form = UserCreationForm(request.POST)
		if form.is_valid():
			user = form.save(commit=False)
			user.is_active = False
			user.save()
			to_email = form.cleaned_data.get('email')
			mail_subject = 'Activate your account.'
			email_message = render_to_string('blog/email_template.html', {
                            'user': user,
                            'domain': request.META['HTTP_HOST'],
                            'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                            'token': account_activation_token.make_token(user),
                        })
			send_mail(mail_subject, email_message, 'muhozagustave1213@gmail.com', [to_email], fail_silently=False, )
			# form.save()
			# username = form.cleaned_data.get('username')
			# messages.info(request, f'Thanks {username},Your account has been created! you can now login!')
			return redirect('login')
	else:
		form = UserCreationForm()
	return render(request, 'blog/register.html', {'form': form})


def activate(request, uidb64, token):
	User = get_user_model()
	try:
		uid = force_text(urlsafe_base64_decode(uidb64))
		user = User.objects.get(pk=uid)
	except(TypeError, ValueError, OverflowError, User.DoesNotExist):
		user = None
	if user is not None and account_activation_token.check_token(user, token):
		user.is_active = True
		user.save()
		return redirect('blog/login.html')
	else:
		return HttpResponse('Activation link is invalid!')



#@login_required
def profile(request):
	if request.method == 'POST':
		u_form = UserUpdateForm(request.POST, instance=request.user)
		p_form = ProfileUpdateForm(request.POST, request.FILES, instance=request.user.profile)
		if u_form.is_valid() and p_form.is_valid():
			u_form.save()
			p_form.save()
			messages.success(request, f'Your account has been updated!')
			return render(request, 'blog/profile.html')

	else:
		u_form = UserUpdateForm(instance=request.user)
		p_form = ProfileUpdateForm(instance=request.user.profile)
	context = {
		'u_form': u_form,
		'p_form': p_form
	}
	return render(request, 'blog/profile.html', context)
"""
message.debug
message.info
message.success
message.warning
message.error
"""